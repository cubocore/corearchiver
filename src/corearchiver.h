/*
    *
    * This file is a part of CoreArchiver.
    * Archive manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QItemSelection>
#include <QCloseEvent>

class CArchiveModel;
class settings;

namespace Ui {
	class corearchiver;
}

class corearchiver : public QWidget {
	Q_OBJECT

public:
	explicit corearchiver(QWidget *parent = nullptr);
	~corearchiver();

	void sendFiles(const QStringList &paths);

protected:
    void closeEvent(QCloseEvent *event) override;

private Q_SLOTS:
	void loadArchive(const QString &fileName);
	void loadFiles(const QStringList &files);

	void compress();
	void extract();

	void on_openArchive_clicked();
	void on_createArchive_clicked();

	void on_addFiles_clicked();
	void on_addFolders_clicked();
	void on_removeFile_clicked();
	void on_arcAction_clicked();
	void on_locationTB_clicked();

private:
    Ui::corearchiver *ui;
    QStringList supportedFormats;
    int uiMode;
    QSize listViewIconSize, toolsIconSize, windowSize;
    bool windowMaximized;
    settings *smi;
	CArchiveModel *model;

	void loadSettings();
	void startSetup();
	void showArchiveMessage();
	void showExtractMessage();
	void updateSelection(const QItemSelection &, const QItemSelection &);
	void updateActions();
    void prepareCreateArchive();
    void prepareLoadArchive();
    bool checkForSpace(const QString &path, const quint64 size);
};
